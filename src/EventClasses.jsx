import React, { Component, Fragment } from "react";
import storageModule from './services/storageModule';

class EventClasses extends Component {

    constructor(props) {
        super(props);
        const rows = [];
        this.state = {
            inputValue: '',
            rows
        };
    }

    onAddButtonClick = event => {
        const { inputValue } = this.state;
        event.preventDefault();
        console.log('Clicked button:', inputValue);
        storageModule().init().then((storage) => {
            const transaction = storage.getDB().transaction('event_classes', 'readwrite');
            // Do something when all the data is added to the database.
            transaction.oncomplete = event => {
                console.log("All done!");
                this.fetchRows();
            };
            transaction.onerror = event => {
                // Don't forget to handle errors!
                console.error("Error adding!");
            };
            const objectStore = transaction.objectStore('event_classes');
            const objectStoreRequest = objectStore.add({ name: inputValue });
            objectStoreRequest.onsuccess = event => {
                console.log('Affiliation added!');
                this.setState({
                    inputValue: ''
                });
            }
        });
    };

    onDeleteButtonClick = (event, row) => {
        const rows = [...this.state.rows];
        const rowsCopy = [...this.state.rows];
        event.preventDefault();
        const index = rows.map(row => row.key).indexOf(row.key);
        if (-1 !== index) {
            rows.splice(index, 1);
            this.setState({
                rows
            });
            storageModule().init().then((storage) => {
                const objectStore = storage.getStore('event_classes', 'readwrite');
                const objectStoreRequest = objectStore.get(row.key);
                objectStoreRequest.onsuccess = event => {
                    console.log("All done:", event.target.result);
                };
                objectStoreRequest.onerror = event => {
                    console.error("Error deleting: ", event.target.errorCode);
                    this.setState({
                        rows: rowsCopy
                    });
                };
                objectStore.delete(row.key);
            });
        }
        console.log("rows", rows);
    };

    onEditButtonClick = (event, row) => {
        const { target } = event;
        const element = target.parentElement.previousElementSibling;
        element.contentEditable = true;
        element.focus();
    };

    onInputChange = event => {
        const { value: inputValue } = event.target;
        this.setState({
            inputValue
        });
    };

    fetchRows = () => {
        storageModule().then((storage) => {
            const store = storage.getStore('event_classes', 'readonly');
            if (!store) {
                return;
            }
            store.openCursor().onsuccess = event => {
                const rows = [...this.state.rows];
                const cursor = event.target.result;
                if (cursor) {
                    console.log("Found event classes: " + cursor.value.name);
                    if (-1 === rows.map(row => row.key).indexOf(cursor.key)) {
                        rows.push({ key: cursor.key, name: cursor.value.name });
                        rows.sort((a, b) => a.name.localeCompare(b.name))
                        this.setState({
                            rows
                        });
                    }
                    cursor.continue();
                }
            };
        });
    }

    componentDidMount() {
        this.fetchRows();
    }

    renderRows = () => {
        const { rows } = this.state;
        return rows.map(row => (
            <tr key={row.key}>
                <td>{row.name}</td>
                <td>
                    <button onClick={event => this.onEditButtonClick(event, row)}>Redigera</button>
                    <button onClick={event => this.onDeleteButtonClick(event, row)}>Ta bort</button>
                </td>
            </tr>
        ));
    };

    render() {
        const { inputValue } = this.state;
        return (
            <Fragment>
                <h2>Tävlingsklasser</h2>
                <table>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
                <input onChange={this.onInputChange} type="text" value={inputValue} />
                <button onClick={this.onAddButtonClick}>Lägg till ny</button>
            </Fragment>
        );
    }
}
export default EventClasses;