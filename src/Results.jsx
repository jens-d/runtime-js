import React, { Component } from "react";

class Results extends Component {

    constructor(props) {
        super(props);
        this.state = {
            familyNameInputValue: '',
            givenNameInputValue: '',
            matches: [],
            resultInputValue: '',
            startNumberInputValue: ''
        };
        this.familyNameInput = React.createRef();
        this.givenNameInput = React.createRef();
    }

    onAddButtonClick = event => {
        const { handleAdd, participants } = this.props;
        const {
            familyNameInputValue,
            givenNameInputValue,
            resultInputValue,
            startNumberInputValue
        } = this.state;
        if (
            !familyNameInputValue ||
            !givenNameInputValue ||
            !resultInputValue ||
            !startNumberInputValue
        ) {
            console.log('Missing field values!');
            return false;
        }
        let eventClassKey;
        let participantKey;

        const familyNameIndex = participants.map(record => record.family_name.toLowerCase()).indexOf(familyNameInputValue.toLowerCase());
        const givenNameIndex = participants.map(record => record.given_name.toLowerCase()).indexOf(givenNameInputValue.toLowerCase());
        if (
            -1 !== familyNameIndex &&
            -1 !== givenNameIndex &&
            familyNameIndex === givenNameIndex
        ) {
            participantKey = participants[familyNameIndex].key;
        } else {
            console.log('Missing participant.');
            return;
        }

        const record = {
            eventClassKey,
            participantKey,
            result: resultInputValue
        }
        if ('function' === typeof handleAdd) {
            handleAdd(record).then(() => {
                this.setState({
                    familyNameInputValue: '',
                    givenNameInputValue: '',
                    resultInputValue: '',
                    startNumberInputValue: ''
                });
            });
        }
    };

    handleDeleteButtonClick = (event, record) => {
        const { handleDelete } = this.props;
        if ('function' === typeof handleDelete) {
            handleDelete(record);
        }
    };

    onNameInputChange = () => {
        let matches = [...this.state.matches];
        const { participants } = this.props;
        let familyNameInputValue, givenNameInputValue;
        if (this.familyNameInput.current) {
            familyNameInputValue = this.familyNameInput.current.value;
        }
        if (this.givenNameInput.current) {
            givenNameInputValue = this.givenNameInput.current.value;
        }
        const familyNameFilter = record => {
            return (new RegExp('^' + familyNameInputValue, 'i')).exec(record.family_name);
        };
        const givenNameFilter = record => {
            return (new RegExp('^' + givenNameInputValue, 'i')).exec(record.given_name);
        };
        const filtered = participants
            .filter(familyNameFilter)
            .filter(givenNameFilter);
        console.log('filtered:', filtered);
        if (
            (
                familyNameInputValue.length > 0 ||
                givenNameInputValue.length > 0
            ) &&
            filtered.length > 0
        ) {
            matches = filtered
                .filter((record, index) =>
                    filtered
                        .map(record => record.key)
                        .indexOf(record.key) === index);
        } else if (
            0 === familyNameInputValue.length &&
            0 === givenNameInputValue.length
        ) {
            matches = [];
        }
        this.setState({
            familyNameInputValue,
            givenNameInputValue,
            matches
        });
    };

    onResultInputValueChange = event => {
        const { value: resultInputValue } = event.target;
        this.setState({
            resultInputValue
        });
    };

    onStartNumberInputValueChange = event => {
        const { value: startNumberInputValue } = event.target;
        this.setState({
            startNumberInputValue
        });
    };

    handleExportButtonClick = () => {
        const { handleExport } = this.props;
        if ('function' === typeof handleExport) {
            handleExport();
        }
    };

    renderMatches = () => {
        const { matches } = this.state;
        return matches.map(record => (
            <li key={record.key}>
                {record.start_number}: {record.family_name}, {record.given_name}
            </li>
        ));
    };

    renderRows = () => {
        const { results } = this.props;

        return results.map(record => (
            <tr key={record.key}>
                <td>
                    {record.participantKey}
                </td>
                <td>
                    {record.given_name}
                </td>
                <td>
                    <button onClick={event => this.handleDeleteButtonClick(event, record)}>Ta bort</button>
                </td>
            </tr>
        ));
    };

    render() {
        const {
            familyNameInputValue,
            givenNameInputValue,
            resultInputValue,
            startNumberInputValue
        } = this.state;

        return (
            <section>
                <h2>Resultat</h2>
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label>
                                    Startnummer:
                    <input onChange={this.onStartNumberInputValueChange} type="text" value={startNumberInputValue} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    Förnamn:
                    <input onChange={this.onNameInputChange} ref={this.givenNameInput} type="text" value={givenNameInputValue} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    Efternamn:
                    <input onChange={this.onNameInputChange} ref={this.familyNameInput} type="text" value={familyNameInputValue} />
                                </label>

                            </td>
                            <td>
                                <label>
                                    Resultat:
                    <input onChange={this.onResultInputValueChange} type="text" value={resultInputValue} />
                                </label>
                            </td>
                            <td>
                                <button onClick={this.onAddButtonClick}>Lägg till ny</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button onClick={this.handleExportButtonClick}>Exportera</button>
                <table>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
                <h3>Sökresultat</h3>
                <ul style={{ "textAlign": "left" }}>
                    {this.renderMatches()}
                </ul>
            </section>
        );
    }
}
export default Results;