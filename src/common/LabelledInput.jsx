import React from 'react'

const LabelledInput = (props, ref) => {
    const { id, label, onChange, type, value } = props

    return (
        <div className="labelled--input">
            <label htmlFor={id}>{label}</label>
            <input
                className="visually-hidden"
                id={id}
                onChange={onChange}
                ref={ref}
                type={type}
                value={value} />
        </div>
    )
}

export default React.forwardRef(LabelledInput)