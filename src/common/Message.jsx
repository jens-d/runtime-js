import React, { Component } from "react";
import '../message.scss'

class Message extends Component {

    state = {
        latest: this.props.message,
        open: 'string' === typeof this.props.message
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.open && 'string' === typeof props.message) {
            return {
                latest: props.message,
                open: true
            };
        }
        if (state.open && 'string' !== typeof props.message) {
            return {
                open: false
            };
        }

        return null;
    }

    typeClassName = () => {
        const { handleClear, type } = this.props;
        const { open } = this.state;
        let classNames = ['message'];

        switch (type) {
            case 'error':
                classNames.push('error');
                break;
            default:
                classNames.push('default');
                break;
        }

        if (open) {
            classNames.push('open');
            setTimeout(() => {
                if ('function' === typeof handleClear) {
                    handleClear();
                }
            }, 3000);
        }

        return classNames.join(' ');
    };

    render() {
        const { message } = this.props;
        const { latest, open } = this.state;

        return (
            <div className={this.typeClassName()}>
                <span>{open ? message : latest}</span>
            </div>
        );
    }
}

export default Message