import React, { Component } from "react";

class Affiliations extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            rows: []
        };
    }

    onAddButtonClick = event => {
        const { inputValue } = this.state;
        if (!inputValue.length) {
            return false;
        }
        const { onAddAffiliation } = this.props;
        if ('function' === typeof onAddAffiliation) {
            onAddAffiliation(inputValue);
        }
        this.setState({
            inputValue: ''
        });
    };

    onDeleteButtonClick = (event, record) => {
        const { onDeleteAffiliation } = this.props;
        if ('function' === typeof onDeleteAffiliation) {
            onDeleteAffiliation(record);
        }
    };

    onEditButtonClick = (event, record) => {
        const { affiliations, onUpdateAffiliation } = this.props;
        const index = affiliations.map(record => record.key).indexOf(record.key);
        if ('function' === typeof onUpdateAffiliation) {
            const affiliation = { ...affiliations[index] }
            affiliation.editable = true;
            onUpdateAffiliation(affiliation);
        }
    };

    onSaveButtonClick = (event, record) => {
        const { affiliations, onUpdateAffiliation } = this.props;
        const index = affiliations.map(record => record.key).indexOf(record.key);
        if ('function' === typeof onUpdateAffiliation) {
            const affiliation = { ...affiliations[index] }
            affiliation.editable = false;
            onUpdateAffiliation(affiliation);
        }
    };

    onInputChange = event => {
        const { value: inputValue } = event.target;
        this.setState({
            inputValue
        });
    };

    onNameInputChange = (event, record) => {
        const { value: inputValue } = event.target;
        if (record) {
            const { affiliations, onUpdateAffiliation } = this.props;
            const index = affiliations.map(record => record.key).indexOf(record.key);
            if ('function' === typeof onUpdateAffiliation) {
                const affiliation = { ...affiliations[index] };
                affiliation.name = inputValue;
                onUpdateAffiliation(affiliation);
            }
        } else {
            this.setState({
                inputValue
            });
        }
    };

    renderAffiliations = () => {
        const { affiliations } = this.props;
        return affiliations.map(record => (
            <tr key={record.key}>
                <td>
                    {record.editable ?
                        <input onChange={event => this.onNameInputChange(event, record)} value={record.name} /> :
                        record.name}
                </td>
                <td>
                    {record.editable ?
                        <button onClick={event => this.onSaveButtonClick(event, record)}>Spara</button> :
                        <button onClick={event => this.onEditButtonClick(event, record)}>Redigera</button>}
                    <button onClick={event => this.onDeleteButtonClick(event, record)}>Ta bort</button>
                </td>
            </tr>
        ));
    };

    render() {
        const { inputValue } = this.state;
        return (
            <section>
                <h2>Föreningar</h2>
                <table>
                    <tbody>
                        {this.renderAffiliations()}
                    </tbody>
                </table>
                <input onChange={this.onInputChange} type="text" value={inputValue} />
                <button onClick={this.onAddButtonClick}>Lägg till ny</button>
            </section>
        );
    }
}
export default Affiliations;