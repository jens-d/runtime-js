import React, { Component } from 'react';
import './App.css';
import './FileUpload.css';
import Affiliations from './Affiliations';
import FileUpload from './FileUpload';
import Participants from './Participants';
import EventClasses from './EventClasses';
import storageModule from './services/storageModule';
import Message from './common/Message';
import Results from './Results';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            affiliations: [],
            eventClasses: [],
            participants: [],
            results: [],
            statusMessage: null,
            statusType: null
        };
    }

    fetchAffiliations = () => {
        const promise = storageModule();
        promise.then(storage => {
            const affiliations = [...this.state.affiliations];
            const store = storage.getStore('affiliations', 'readonly');
            store.openCursor().onsuccess = event => {
                const cursor = event.target.result;
                if (cursor) {
                    if (-1 === affiliations.map(record => record.key).indexOf(cursor.key)) {
                        affiliations.push({ key: cursor.key, name: cursor.value.name });
                        affiliations.sort((a, b) => a.name.localeCompare(b.name));
                    }
                    cursor.continue();
                } else {
                    this.setState({
                        affiliations
                    });
                }
            };
        });

        return promise;
    };

    fetchParticipants = () => {
        const promise = storageModule();
        promise.then(storage => {
            const participants = [...this.state.participants];
            const store = storage.getStore('participants', 'readonly');
            store.openCursor().onsuccess = event => {
                const cursor = event.target.result;
                if (cursor) {
                    if (-1 === participants.map(record => record.key).indexOf(cursor.key)) {
                        participants.push({
                            affiliation: cursor.value.affiliation,
                            key: cursor.key,
                            family_name: cursor.value.family_name,
                            given_name: cursor.value.given_name
                        });
                        participants.sort((a, b) => a.family_name.localeCompare(b.family_name));
                    }
                    cursor.continue();
                } else {
                    this.setState({
                        participants
                    });
                }
            };
        });

        return promise;
    };

    fetchResults = () => {
        const promise = storageModule();
        promise.then(storage => {
            const results = [...this.state.results];
            const store = storage.getStore('results', 'readonly');
            store.openCursor().onsuccess = event => {
                const cursor = event.target.result;
                if (cursor) {
                    if (-1 === results.map(record => record.key).indexOf(cursor.key)) {
                        results.push({
                            eventClassKey: cursor.value.event_class_key,
                            key: cursor.key,
                            participantKey: cursor.value.participant_key,
                            result: cursor.value.result
                        });
                        results.sort((a, b) => a.sort_order > b.sort_order);
                    }
                    cursor.continue();
                } else {
                    this.setState({
                        results
                    });
                }
            };
        });

        return promise;
    };

    handleAddAffiliation = affiliationName => {
        const promise = storageModule();
        promise.then(storage => {
            const transaction = storage.getDB().transaction('affiliations', 'readwrite');
            // Do something when all the data is added to the database.
            transaction.addEventListener('complete', event => {
                console.log("Transaction done:", event);
            });
            transaction.addEventListener('error', event => {
                // Don't forget to handle errors!
                console.error("Transaction error:", event.target.error);
            });
            const objectStore = transaction.objectStore('affiliations');
            const objectStoreRequest = objectStore.add({ name: affiliationName });
            objectStoreRequest.addEventListener('success', event => {
                console.log('Affiliation added!', event.target.result);
                this.setState({
                    statusMessage: 'Föreningen tillagd!'
                });
                this.fetchAffiliations();
            });
            objectStoreRequest.addEventListener('error', event => {
                console.error("Error adding affiliation:", event.target.error.message);
                this.setState({
                    statusMessage: 'Det gick inte att lägga till föreningen!',
                    statusType: 'error'
                });
            });
        });

        return promise;
    };

    handleDeleteAffiliation = affiliation => {
        storageModule().then(storage => {
            const store = storage.getStore('affiliations', 'readwrite');
            const getRequest = store.get(affiliation.key);
            getRequest.onsuccess = event => {
                const record = event.target.result;
                if ('undefined' === typeof record) {
                    return;
                }
                const deleteRequest = store.delete(affiliation.key);
                deleteRequest.onsuccess = event => {
                    console.log("All done:", event);
                    const affiliations = [...this.state.affiliations];
                    const index = affiliations.map(record => record.key).indexOf(affiliation.key);
                    affiliations.splice(index, 1);
                    this.setState({
                        affiliations,
                        statusMessage: 'Tog bort föreningen!'
                    });
                };
                deleteRequest.onerror = event => {
                    console.error("Error deleting: ", event.target.errorCode);
                };
            };
            getRequest.onerror = event => {
                console.error("Error getting: ", event.target.errorCode);
            };
        });
    };

    handleUpdateAffiliation = affiliation => {
        storageModule().then(storage => {
            const store = storage.getStore('affiliations', 'readwrite');
            const getRequest = store.get(affiliation.key);
            getRequest.onsuccess = event => {
                const record = event.target.result;
                if ('undefined' === typeof record) {
                    return;
                }
                record.name = affiliation.name;
                const putRequest = store.put(record);
                putRequest.onerror = event => {
                    console.error('Error updating object!');
                };
                putRequest.onsuccess = event => {
                    console.log('Updated object!');
                    const affiliations = [...this.state.affiliations];
                    const index = affiliations.map(record => record.key).indexOf(affiliation.key);
                    affiliations[index] = { ...affiliation };
                    this.setState({
                        affiliations
                    })
                };
            };
            getRequest.onerror = event => {
                console.error("Error getting: ", event.target.errorCode);
            };
        });
    };

    handleAddParticipant = participant => {
        storageModule().then(storage => {
            const transaction = storage.getDB().transaction('participants', 'readwrite');
            // Do something when all the data is added to the database.
            transaction.oncomplete = event => {
                console.log("All done!");
            };
            transaction.onerror = event => {
                // Don't forget to handle errors!
                console.error("Error adding!");
            };
            const objectStore = transaction.objectStore('participants');
            const objectStoreRequest = objectStore.add({ family_name: participant.family_name, given_name: participant.given_name });
            objectStoreRequest.onsuccess = event => {
                console.log('Affiliation added!');
                const participants = [...this.state.participants];
                participants.push(participant);
                this.setState({
                    participants
                });
            }
        });
    };

    handleDeleteParticipant = participant => {
        storageModule().then(storage => {
            const objectStore = storage.getStore('participants', 'readwrite');
            const objectStoreRequest = objectStore.get(participant.key);
            objectStoreRequest.onsuccess = event => {
                console.log("Deleted participant:", event.target.result);
                const participants = [...this.state.participants];
                const index = participants.map(record => record.key).indexOf(participant.key);
                participants.splice(index, 1);
                this.setState({
                    participants
                });
            };
            objectStoreRequest.onerror = event => {
                console.error("Error deleting: ", event.target.errorCode);
            };
            objectStore.delete(participant.key);
        });
    };

    handleSaveParticipant = participant => {
        storageModule().then(storage => {
            const store = storage.getStore('participants', 'readwrite');
            if (!store) {
                return;
            }
            console.log('Found participant:', participant.key);
            const request = store.get(participant.key);
            request.onerror = event => {
                // Handle errors!
                console.error('Error getting object!');
            };
            request.onsuccess = event => {
                // Get the old value that we want to update
                const record = event.target.result;
                console.log('Found data:', record);

                // update the value(s) in the object that you want to change
                record.affiliation = participant.affiliation;
                record.family_name = participant.family_name;
                record.given_name = participant.given_name;

                // Put this updated object back into the database.
                const requestUpdate = store.put(record, participant.key);
                requestUpdate.onerror = event => {
                    // Do something with the error
                    console.error('Error updating object!');
                };
                requestUpdate.onsuccess = event => {
                    // Success - the data is updated!
                    console.log('Updated object!');
                    const participants = [...this.state.participants];
                    const index = participants.map(record => record.key).indexOf(participant.key);
                    participants[index] = { ...participant };
                    this.setState({
                        participants
                    });
                };
            };
        });
    };

    handleUpdateParticipant = participant => {
        const participants = [...this.state.participants];
        const index = participants.map(record => record.key).indexOf(participant.key);
        participants[index] = { ...participant };
        this.setState({
            participants
        });
    };

    handleAddResult = record =>
        new Promise((resolve, reject) => {
            console.log('Adding record:', record);
            storageModule().then(storage => {
                const store = storage.getStore('results', 'readwrite');
                store.transaction.addEventListener('complete', event => {
                    console.log('Add result transaction complete:', event);
                });
                store.transaction.addEventListener('error', event => {
                    console.error("Error on result transaction:", event.target.error.message);
                });
                store.add({
                    event_class_key: record.eventClassKey,
                    participant_key: record.participantKey,
                    result: record.result,
                    sort_order: 0
                });
            });
        });

    handleDeleteResult = record =>
        new Promise((resolve, reject) => {
            console.log('Deleting record:', record);
            storageModule().then(storage => {
                const store = storage.getStore('results', 'readwrite');
                const request = store.get(record.key);
                request.onsuccess = event => {
                    console.log("Deleted result:", event.target.result);
                    const results = [...this.state.results];
                    const index = results.map(record => record.key).indexOf(record.key);
                    results.splice(index, 1);
                    this.setState({
                        results
                    });
                };
                store.onerror = event => {
                    console.error("Error deleting result: ", event.target.errorCode);
                };
                store.delete(record.key);
            });
        });

    handleImportParticipants = records =>
        new Promise((resolve, reject) => {

            const addAffiliations = () => {
                storageModule().then(storage => {
                    const affiliations = [...this.state.affiliations];
                    const transaction = storage.getDB().transaction(['affiliations'], 'readwrite');
                    transaction.addEventListener('complete', event => {
                        console.log('Add affiliation transaction complete:', event);
                        this.fetchParticipants().then(storage => {
                            const { affiliations } = this.state;
                            const participants = [...this.state.participants];
                            const store = storage.getStore('participants', 'readwrite');
                            store.transaction.addEventListener('complete', event => {
                                console.log('Add participant transaction complete:', event);
                                resolve([...this.state.participants]);
                            });
                            records.forEach(record => {
                                let participant;
                                console.log('Found record...', record);
                                if (
                                    'string' === typeof record.family_name &&
                                    'string' === typeof record.given_name
                                ) {
                                    const familyNameIndex = participants.map(record => record.family_name).indexOf(record.family_name);
                                    const givenNameIndex = participants.map(record => record.given_name).indexOf(record.given_name);
                                    if (
                                        -1 !== familyNameIndex &&
                                        -1 !== givenNameIndex &&
                                        familyNameIndex === givenNameIndex
                                    ) {
                                        console.log('Found participant', participants[familyNameIndex]);
                                        participant = { ...participants[familyNameIndex] };
                                    } else {
                                        participant = {};
                                        participant.family_name = record.family_name;
                                        participant.given_name = record.given_name;
                                    }
                                }
                                const affiliationIndex = affiliations.map(record => record.key).indexOf(record.affiliation);
                                if (-1 === affiliationIndex) {
                                    participant.affiliation = '';
                                } else {
                                    participant.affiliation = { ...affiliations[affiliationIndex] };
                                }
                                const request = store.add(participant);
                                console.log('Participant transaction:', request.transaction);
                                request.addEventListener('success', event => {
                                    participant.key = event.target.result;
                                    console.log('Participant added:', participant);
                                    participants.push(participant);
                                    participants.sort((a, b) => a.family_name.localeCompare(b.family_name))
                                    this.setState({
                                        participants
                                    });
                                });
                                request.addEventListener('error', event => {
                                    console.error("Error adding participant:", event.target.error.message);
                                });
                            });
                        });
                    });
                    transaction.addEventListener('error', event => {
                        console.error("Error on affiliation transaction:", event.target.error.message);
                    });
                    records.forEach(record => {
                        if ('string' !== typeof record.affiliation) {
                            return;
                        }
                        console.log('Adding affiliation...', record.affiliation);
                        const request = transaction
                            .objectStore('affiliations')
                            .add({
                                name: record.affiliation
                            });
                        request.addEventListener('success', event => {
                            console.log('Added affiliation...', event.target.result);
                            const index = affiliations.map(record => record.key).indexOf(event.target.result);
                            if (-1 === index) {
                                affiliations.push({
                                    key: event.target.result,
                                    name: event.target.result
                                });
                                affiliations.sort((a, b) => a.name.localeCompare(b.name));
                                this.setState({
                                    affiliations
                                });
                            } else {
                                console.log('Found affiliation...', affiliations[index]);
                            }
                        });
                        request.addEventListener('error', event => {
                            event.preventDefault();
                            console.log("Error adding affiliation:", event.target.error.message);
                        });
                    });
                });
            };
            this.fetchAffiliations().then(() => {
                addAffiliations();
            });
        });

    handleClearStatus = () => {
        this.setState({
            statusMessage: null,
            statusType: null
        });
    }

    handleExportParticipants = () => {
        const [...participants] = this.state.participants;
        const data = participants.map(participant => [
            participant.family_name,
            participant.given_name
        ].join(';')).join("\n");
        const encoded = window.btoa(data);
        const csv = `data:application/octet-stream;base64,${encoded}`;
        console.log('exporting...', csv);

        const element = document.createElement('a');
        element.setAttribute('href', csv);
        element.setAttribute('download', 'participants.csv');

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

    componentDidMount() {
        this.fetchAffiliations();
        this.fetchParticipants();
        this.fetchResults();
    }

    render() {
        const {
            affiliations,
            eventClasses,
            participants,
            results,
            statusMessage,
            statusType
        } = this.state;
        return (
            <div className="App">
                <Message handleClear={this.handleClearStatus} message={statusMessage} type={statusType} />
                <section>
                    <Results
                        handleAdd={this.handleAddResult}
                        handleDelete={this.handleDeleteResult}
                        participants={participants}
                        results={results} />
                    <Affiliations
                        affiliations={affiliations}
                        onAddAffiliation={this.handleAddAffiliation}
                        onDeleteAffiliation={this.handleDeleteAffiliation}
                        onUpdateAffiliation={this.handleUpdateAffiliation} />
                    <EventClasses eventClasses={eventClasses} />
                    <Participants
                        affiliations={affiliations}
                        handleExport={this.handleExportParticipants}
                        participants={participants}
                        onAddParticipant={this.handleAddParticipant}
                        onDeleteParticipant={this.handleDeleteParticipant}
                        onUpdateParticipant={this.handleUpdateParticipant}
                        onSaveParticipant={this.handleSaveParticipant} />
                    <FileUpload
                        handleImport={this.handleImportParticipants} />
                </section>
            </div>
        );
    }
}

export default App;
