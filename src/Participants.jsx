import React, { Component } from "react";

class Participants extends Component {

    constructor(props) {
        super(props);
        this.state = {
            familyNameInputValue: '',
            givenNameInputValue: '',
            startNumberInputValue: ''
        };
    }

    onAddButtonClick = event => {
        const { onAddParticipant } = this.props;
        const { familyNameInputValue, givenNameInputValue } = this.state;
        if (!familyNameInputValue || !givenNameInputValue) {
            console.log('Missing names!');
            return false;
        }
        const participant = {
            family_name: familyNameInputValue,
            given_name: givenNameInputValue
        }
        if ('function' === typeof onAddParticipant) {
            onAddParticipant(participant);
            this.setState({
                familyNameInputValue: '',
                givenNameInputValue: ''
            });
        }
    };

    onDeleteButtonClick = (event, record) => {
        const { onDeleteParticipant } = this.props;
        if ('function' === typeof onDeleteParticipant) {
            onDeleteParticipant(record);
        }
    };

    onEditButtonClick = (event, record) => {
        const { onUpdateParticipant, participants } = this.props;
        const index = participants.map(record => record.key).indexOf(record.key);
        const participant = { ...participants[index] };
        participant.editable = true;
        if ('function' === typeof onUpdateParticipant) {
            onUpdateParticipant(participant);
        }
    };

    onSaveButtonClick = (event, record) => {
        const { onSaveParticipant, participants } = this.props;
        const index = participants.map(record => record.key).indexOf(record.key);
        const participant = { ...participants[index] };
        participant.family_name = record.family_name;
        participant.given_name = record.given_name;
        participant.editable = false;
        if ('function' === typeof onSaveParticipant) {
            onSaveParticipant(participant);
        }
    };

    onStartNumberInputChange = (event, record) => {
        const { value: startNumberInputValue } = event.target;
        const { onUpdateParticipant } = this.props;
        if (record) {
            const participant = { ...record };
            participant.start_number = startNumberInputValue;
            if ('function' === typeof onUpdateParticipant) {
                onUpdateParticipant(participant);
            }
        } else {
            this.setState({
                startNumberInputValue
            });
        }
    };

    onFamilyNameInputChange = (event, record) => {
        const { value: familyNameInputValue } = event.target;
        const { onUpdateParticipant } = this.props;
        if (record) {
            const participant = { ...record };
            participant.family_name = familyNameInputValue;
            if ('function' === typeof onUpdateParticipant) {
                onUpdateParticipant(participant);
            }
        } else {
            this.setState({
                familyNameInputValue
            });
        }
    };

    onGivenNameInputChange = (event, record) => {
        const { value: givenNameInputValue } = event.target;
        const { onUpdateParticipant } = this.props;
        if (record) {
            const participant = { ...record };
            participant.given_name = givenNameInputValue;
            if ('function' === typeof onUpdateParticipant) {
                onUpdateParticipant(participant);
            }
        } else {
            this.setState({
                givenNameInputValue
            });
        }
    };

    onAffiliationSelectorChange = (event, record) => {
        const { affiliations, onUpdateParticipant } = this.props;
        const { value: affiliationName } = event.target;
        if (affiliationName) {
            const index = affiliations.map(record => record.key).indexOf(affiliationName);
            if (-1 !== index && 'function' === typeof onUpdateParticipant) {
                const participant = { ...record };
                participant.affiliation = affiliations[index];
                onUpdateParticipant(participant);
            }
        }
    };

    handleExportButtonClick = () => {
        const { handleExport } = this.props;
        if ('function' === typeof handleExport) {
            handleExport();
        }
    };

    renderAffiliationSelector = record => {
        const { affiliations } = this.props;
        const options = affiliations.map(affiliation => (
            <option
                key={affiliation.key}
                value={affiliation.key}>{affiliation.name}</option>
        ));

        return (
            <select defaultValue={record.affiliation ? record.affiliation.key : null}
                onChange={event => this.onAffiliationSelectorChange(event, record)}>{options}</select>
        );
    };

    renderRows = () => {
        const { participants } = this.props;

        return participants.map(record => (
            <tr key={record.key}>
                <td>
                    {record.editable ? <input onChange={event => this.onStartNumberInputChange(event, record)} value={record.start_number} /> : record.start_number}
                </td>
                <td>
                    {record.editable ? <input onChange={event => this.onFamilyNameInputChange(event, record)} value={record.family_name} /> : record.family_name}
                </td>
                <td>
                    {record.editable ? <input onChange={event => this.onGivenNameInputChange(event, record)} value={record.given_name} /> : record.given_name}
                </td>
                <td>
                    {record.editable ? this.renderAffiliationSelector(record) : (record.affiliation ? record.affiliation.name : '')}
                </td>
                <td>
                    {record.editable ?
                        <button onClick={event => this.onSaveButtonClick(event, record)}>Spara</button> :
                        <button onClick={event => this.onEditButtonClick(event, record)}>Redigera</button>}
                    <button onClick={event => this.onDeleteButtonClick(event, record)}>Ta bort</button>
                </td>
            </tr>
        ));
    };

    render() {
        const { familyNameInputValue, givenNameInputValue } = this.state;

        return (
            <section>
                <h2>Deltagare</h2>
                <button onClick={this.handleExportButtonClick}>Exportera</button>
                <table>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
                <label>
                    Förnamn:
                    <input onChange={this.onGivenNameInputChange} type="text" value={givenNameInputValue} />
                </label>
                <label>
                    Efternamn:
                    <input onChange={this.onFamilyNameInputChange} type="text" value={familyNameInputValue} />
                </label>
                <button onClick={this.onAddButtonClick}>Lägg till ny</button>
            </section>
        );
    }
}
export default Participants;