import React, { Component, Fragment } from "react";
import LabelledInput from "./common/LabelledInput";
import stringParserService from './services/stringParserService';

class FileUpload extends Component {

    constructor(props) {
        super(props);
        this.fileElement = React.createRef();
        this.state = {
            records: []
        };
    }

    handleFileInputChange = () => {
        const { files } = this.fileElement.current;
        const self = this;
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            if ('' !== file.type && 'text/plain' !== file.type) {
                continue;
            }
            const reader = new FileReader();
            reader.onload = (function () {
                return e => {
                    let csv;
                    try {
                        csv = stringParserService()
                            .fromString(e.target.result)
                            .parseCSV();
                    } catch (e) {
                        csv = [];
                    }
                    const records = [];
                    for (let i = 0; i < csv.length; i++) {
                        const record = csv[i];
                        for (let j = 0; j < record.length; j++) {
                            if (record[j][0] > 0) {
                                records.push({
                                    place: record[j][0],
                                    start_number: record[j][1],
                                    family_name: record[j][3],
                                    given_name: record[j][4],
                                    affiliation: record[j][5],
                                    time: record[j][6],
                                    delta_time: record[j][7]
                                })
                            }
                        }
                    }
                    console.log('csv:', csv);
                    console.log('records:', records);
                    self.setState({
                        records
                    })
                };
            })();
            reader.readAsText(file);
        }
    }

    handleImportButtonClick = () => {
        const { records } = this.state;
        const { handleImport } = this.props;
        if (!records.length) {
            return;
        }
        if ('function' === typeof handleImport) {
            console.log('Importing records...', records);
            handleImport(records).then(finalRecords => {
                console.log('Importing done!', finalRecords);
                this.setState({
                    records: []
                });
            });
        }
    };

    componentDidMount() {
        //
    }

    renderImportRecords = () => {
        const records = [...this.state.records];
        if (!records.length) {
            return;
        }
        records.sort((a, b) => a.family_name.localeCompare(b.family_name))
        return (
            <section>
                <table width="100%">
                    <thead>
                        <tr>
                            <td align="left">Efternamn</td>
                            <td align="left">Förnamn</td>
                            <td align="left">Förening</td>
                        </tr>
                    </thead>
                    <tbody>
                        {records.map((row, index) => (
                            <tr key={index}>
                                <td align="left">{row.family_name}</td>
                                <td align="left">{row.given_name}</td>
                                <td align="left">{row.affiliation}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button onClick={this.handleImportButtonClick}>Importera</button>
            </section>
        );
    };

    render() {
        const { records } = this.state;
        return (
            <Fragment>
                <h2>Importera deltagare</h2>
                {records.length ? this.renderImportRecords() : (
                    <LabelledInput
                        id="fileElement"
                        label="Välj en CSV-fil"
                        onChange={this.handleFileInputChange}
                        ref={this.fileElement}
                        type="file" />
                )}
            </Fragment>
        );
    }
}
export default FileUpload;