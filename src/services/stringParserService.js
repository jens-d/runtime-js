const stringParser = () => {

    let sourceString;

    const validateString = string => 'string' === typeof string && string.length > 0;
    const validateSource = () => {
        if (!validateString(sourceString)) {
            throw new Error('Missing source data to process.');
        }
    }

    const settings = {
        csv: {
            columnDelimiter: ',',
            rowDelimiter: "\r\n",
            stringDelimiter: ''
        },
        useHeaders: true
    };

    const module = {
        useHeaders: (useHeaders = true) => {
            settings.useHeaders = !!useHeaders;

            return module;
        },

        fromString: string => {
            if (validateString(string)) {
                sourceString = string;
            }

            return module;
        },

        /**
         * Try to parse the source data as CSV.
         * @returns array
         */
        parseCSV: () => {
            validateSource();
            const { columnDelimiter, rowDelimiter } = settings.csv;
            const records = [];
            if (
                -1 === sourceString.indexOf(rowDelimiter) ||
                -1 === sourceString.indexOf(columnDelimiter)
            ) {
                return records;
            }
            const rows = sourceString.split(rowDelimiter);
            console.log(`Found ${rows.length} rows`);
            let i = settings.useHeaders ? 0 : 1;
            for (i; i < rows.length; i++) {
                if (-1 === rows[i].indexOf(columnDelimiter)) {
                    continue;
                }
                const columns = rows[i].split(columnDelimiter);
                const row = []
                for (let j = 0; j < columns.length; j++) {
                    const column = columns[j].trim();
                    row.push(column);
                }
                records.push([row]);
            }

            return records;
        }
    };

    return module;
}

export default stringParser;