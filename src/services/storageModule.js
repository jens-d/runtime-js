const storageModule = () => {
    let db;

    const module = {
        init: () => new Promise((resolve, reject) => {
            if (db) {
                resolve(module);
            }
            if (!window.indexedDB) {
                console.error("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
                return;
            }
            // Let us open our database
            const request = window.indexedDB.open('events', 1);
            request.onerror = event => {
                // Do something with request.errorCode!
                console.error("Database error: " + event.target.errorCode);
                console.log("Why didn't you allow my web app to use IndexedDB?!");
                reject(event.target.errorCode);
            };
            request.onsuccess = event => {
                db = event.target.result;
                resolve(module);
            };
            request.onupgradeneeded = event => {
                // Do something with request.result!
                console.log("We need a new IndexedDB!");
                db = event.target.result;
                db.onerror = event => {
                    // Generic error handler for all errors targeted at this database's
                    // requests!
                    console.error("Database error: " + event.target.errorCode);
                };

                // Create an affiliations objectStore for this database
                let objectStore = db.createObjectStore('affiliations', { keyPath: 'name' });
                objectStore.createIndex('name', 'name', { unique: true });

                // Create an participants objectStore for this database
                objectStore = db.createObjectStore('participants', { autoIncrement: true });
                objectStore.createIndex('family_name', 'family_name', { unique: false });
                objectStore.createIndex('given_name', 'given_name', { unique: false });
                objectStore.createIndex('start_number', 'start_number', { unique: false });
                objectStore.createIndex('affiliation', 'affiliation', { unique: false });

                // Create an event classes objectStore for this database
                objectStore = db.createObjectStore('event_classes', { autoIncrement: true });
                objectStore.createIndex('name', 'name', { unique: true });
                objectStore.createIndex('sort_order', 'sort_order', { unique: false });

                // Create an event classes objectStore for this database
                objectStore = db.createObjectStore('results', { autoIncrement: true });
                objectStore.createIndex('event_class_key', 'event_class_key', { unique: false });
                objectStore.createIndex('participant_key', 'participant_key', { unique: false });
                objectStore.createIndex('result', 'result', { unique: false });
                objectStore.createIndex('sort_order', 'sort_order', { unique: false });

                // Use transaction oncomplete to make sure the objectStore creation is 
                // finished before adding data into it.
                objectStore.transaction.oncomplete = () => {
                    // Store values in the newly created objectStore.
                    let objectStore = module.getStore('affiliations', 'readwrite');
                    let objectStoreRequest;
                    [{ name: "Rånäs 4H" }].forEach(record => {
                        // TODO: Why aren't affiliations saved?
                        objectStoreRequest = objectStore.add(record);
                        objectStoreRequest.onsuccess = event => {
                            console.log('Affiliation added!');
                        }
                    });

                    objectStore = module.getStore('participants', 'readwrite');
                    [{ family_name: "Davidsson", given_name: "Jens" }].forEach(record => {
                        objectStoreRequest = objectStore.add(record);
                        objectStoreRequest.onsuccess = event => {
                            console.log('Participant added!');
                        }
                    });

                    objectStore = module.getStore('event_classes', 'readwrite');
                    [{ name: "Män, 10 km", sort_order: 0 }].forEach(record => {
                        objectStoreRequest = objectStore.add(record);
                        objectStoreRequest.onsuccess = event => {
                            console.log('Event class added!');
                        }
                    });
                };
                objectStore.transaction.onerror = event => {
                    console.error("objectStore transaction error: " + event.target.errorCode);
                };
            };
            // https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Using_IndexedDB
        }),
        getDB: () => db,
        getStore: (storeName, mode) => {
            if (!module.getDB()) {
                return false;
            }
            const transaction = db.transaction(storeName, mode);
            const objectStore = transaction.objectStore(storeName);

            return objectStore;
        }
    }

    return module.init();
};

export default storageModule;